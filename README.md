## Camera Tampering

#### This module detect cases of Occlusion, Deviation, Defocus, Blackout/Noise or No Video. Various edge cases is being handled using Image Processing Techniques. 

### Setup
```python
pip3 install requirements.txt

```
### Run
```python
python3 main.py
```

### Now Browse
- [http://127.0.0.1:5000/](http://127.0.0.1:5000/)

#### Please provide the source accordingly
```python
cv2.VideoCapture(0)

```
## GUI

<p align="center"><img src="./Output/GUI.png" width=640></p><br>



## Normal

<p align="center"><img src="./Output/Normal.jpg" width=640></p><br>

## Occulsion

<p align="center"><img src="./Output/Occulsion.jpg" width=640></p><br>

## Deviation

<p align="center"><img src="./Output/Deviation.jpg" width=640></p><br>

## Defocus

<p align="center"><img src="./Output/Defocus.jpg" width=640></p><br>
