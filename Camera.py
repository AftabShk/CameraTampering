import cv2
import CameraTamperingModule
Camera = CameraTamperingModule.Camera()

class VideoCamera(object):
    def __init__(self):
       #capturing video
       self.video = cv2.VideoCapture('utils/testing/videos/video1.mp4')
    
    def __del__(self):
        #releasing camera
        self.video.release()
        
    def get_frame(self):
       #extracting frames
        ret, frame = self.video.read()
        frame = Camera.update(frame)
        ret, jpeg = cv2.imencode('.jpg', frame)
        return jpeg.tobytes()