import cv2
import numpy as np
from threading import Thread

class Camera:
    def __init__(self):

        self.backSub = cv2.createBackgroundSubtractorMOG2()

        self.bg_frame_count = 0 
        # if there is a change in the bg bg_frame_count increments by 1 and return True if the count has passed the threshold(i have kept it 1). refer line no. 105,106
        self.bg_hist_frame = None 
        # it store previous frame which can be compared with the latest frame to check whether the tampering is still present or not. 
        self.bg_hist_count = 0
        # It is count for previous frame, check line 85 to 89. we store every 5th frame.  
        self.bg_change_hist = False
        # It is used to tell whether the tampered bg has been changed or not. If it changes we perform bg subtraction, if the tampered does not changes we compare the current frame with bg_hist_frame


    def DetectBlurFft(self,image, size=60, defocus_thresh=10):
        image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        image = cv2.equalizeHist(image)
        (h, w) = image.shape
        (cX, cY) = (int(w / 2.0), int(h / 2.0))
        fft = np.fft.fft2(image)
        fftShift = np.fft.fftshift(fft)
        fftShift[cY - size:cY + size, cX - size:cX + size] = 0
        fftShift = np.fft.ifftshift(fftShift)
        recon = np.fft.ifft2(fftShift)
        magnitude = 20 * np.log(np.abs(recon))
        mean = np.mean(magnitude)
        return (mean, mean <= defocus_thresh)

    def checkUP_bg(self,frame):
        if self.bg_change_hist:
            halfPixel,diff = self.compare_bg([frame,self.bg_hist_frame])
            # cv2.imwrite('frame.jpg',frame)
            # cv2.imwrite('frame2.jpg',self.bg_hist_frame)
            # print('score',halfPixel,diff)
            if diff > halfPixel:
                # print('frame matched')
                self.bg_change_hist = True
            else:
                # print('frame Does not Matched')
                self.bg_change_hist = False

    def BgSubtraction(self,org_frame,bg_thresh,bg_hist_frame_limit,bg_change_limit):
        if self.bg_hist_count == bg_hist_frame_limit:
            self.bg_hist_frame = org_frame
            self.bg_hist_count = 0
        else:
            self.bg_hist_count+=1

        if not self.bg_change_hist:

            # Applied background subtraction 
            self.fgMask = self.backSub.apply(org_frame)   
            #the changed object is represented as white pixel
            #..and if nothing changes in the background then
            #the output is in black color 
            whitePixel = (self.fgMask == 255).sum()

            # Total pixel in the frame
            totalPixel = self.fgMask.shape[0] * self.fgMask.shape[1]

            # percentage of background changed
            percentChange = whitePixel / totalPixel
            # print('percentage change',percentChange)
            #Threshold for amount of frame changed from previous frame
            if percentChange > bg_thresh:
                #increase the count.
                self.bg_frame_count+=1
                #if the count is greater than limit.
                if self.bg_frame_count > bg_change_limit:
                    # we set History change for ture so that we can compare the current frame with bg_hist_frame
                    self.bg_change_hist = True
                    return (percentChange,True)
                else:
                    return (percentChange,False)
            else:
                return (percentChange,False)
        else:
            # If False which means After Image comparison there is still tampering.
            return (0,True)

    def compare_bg(self,frame):
        f1 = cv2.cvtColor(frame[0], cv2.COLOR_BGR2GRAY)
        f2 = cv2.cvtColor(frame[1], cv2.COLOR_BGR2GRAY)
        threshold = 0.1

        absdiff = cv2.absdiff(f1, f2)
        _, thresholded = cv2.threshold(
          absdiff, int(threshold * 255),
          255, cv2.THRESH_BINARY)
        Diff  = np.count_nonzero(thresholded)
        return (f1.shape[0] * f1.shape[1])/2,Diff

    def CheckBlackoutNoiseNoVideo(self,frame,p=0.05,s_thr=0.2):

        blackPixel = (frame == 0).sum()
        totalPixel = frame.shape[0] * frame.shape[1]
        percentChange = blackPixel / totalPixel
        # print('percentage change',percentChange)

        if percentChange > 1:
            # Noise blackout Video
            image = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
            s = cv2.calcHist([image], [1], None, [256], [0, 256])
            p = 0.09
            s_perc = np.sum(s[int(p * 255):-1]) / np.prod(image.shape[0:2])
            s_thr = 0.2
            print(s_perc,s_thr)
            # print(s.sum())
            if s_perc > s_thr:
                return True
            else:
                return False
        else:
            return True

    def update(self,frame):
        self.org_frame = frame.copy()
        self.org_frame = cv2.resize(self.org_frame,(640, 480))
        # print('resized')
        if self.CheckBlackoutNoiseNoVideo(self.org_frame,p=0.05,s_thr=0.2): 
        # if there is any noise or blackout we don't proceed for Tampering Detection
            (value_bg, t_or_f_bg) = self.BgSubtraction(self.org_frame,bg_thresh=0.35,bg_hist_frame_limit=3,bg_change_limit=10)
            # We will check for image comparison if the frame is matched that means tampering is still going.
            self.checkUP_bg(self.org_frame)

            # we check for Defocus
            (value_defocus,t_or_f_defocus) = self.DetectBlurFft(self.org_frame,size=60,defocus_thresh=10)
            # print(value,t_or_f)

            color_bg = (0, 0, 255) if t_or_f_bg else (0, 255, 0)
            color_defocus = (0, 0, 255) if t_or_f_defocus else (0, 255, 0)

            text_bg = "Occlusion or Displacement Tampering ({:.4f})" if t_or_f_bg else "No Tampering ({:.4f})"
            text_bg = text_bg.format(value_bg)

            text_defocus = "Defocus Tampering ({:.4f})" if t_or_f_defocus else "No Tampering ({:.4f})"
            text_defocus = text_defocus.format(value_defocus)
            
            cv2.putText(frame, text_bg, (10, 40), cv2.FONT_HERSHEY_SIMPLEX,2, color_bg, 4)
            cv2.putText(frame, text_defocus, (10, 90), cv2.FONT_HERSHEY_SIMPLEX,2, color_defocus, 4)
        return frame


# if __name__ == "__main__":
#     camera = Camera('utils/testing/videos/video1.mp4')
#     camera.update()
